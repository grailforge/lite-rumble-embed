/**
 * A lightweight Rumble embed.
 **/

class LiteRumbleEmbed extends HTMLElement {

    connectedCallback() {
        const DEFAULT_WIDTH = "560px";
        const DEFAULT_HEIGHT = "315px"
 
        this.videoId = this.getAttribute('videoid');
        this.videoUrl =  `https://rumble.com/${this.videoId}`;
        this.embedId = this.getAttribute('embedid');
        this.embedUrl = `https://rumble.com/embed/${encodeURIComponent(this.embedId)}?}`;
 
        this.width = this.getAttribute('width') || DEFAULT_WIDTH;
        this.height = this.getAttribute('height') || DEFAULT_HEIGHT;
        this.style.width = this.width;
        this.style.height = this.height;

        // This allows <lite-rumble classicbutton> shorthand to be the same as <lite-rumble classicbutton="true">
        const classicButton = this.getAttribute('classicButton');
        this.useClassicPlayButton = !(classicButton === null || classicButton === undefined || classicButton == "false" || classicButton === false);

        // Likewise for <lite-rumble noinline>
        const noinline = this.getAttribute('noinline');
        this.noinline = !(noinline === null || noinline === undefined || noinline == "false" || noinline === false);

        let playBtnEl = this.querySelector('.lrmb-playbtn');
        // A label for the button takes priority over a [playlabel] attribute on the custom-element
        this.playLabel = (playBtnEl && playBtnEl.textContent.trim()) || this.getAttribute('playlabel') || 'Play';
        this.dataset.title = this.getAttribute('title') || "";

        // Set up play button, and its visually hidden label
        if (!playBtnEl) {
            playBtnEl = document.createElement('button');
            playBtnEl.type = 'button';
            playBtnEl.classList.add('lrmb-playbtn');
            if(this.useClassicPlayButton) {
                playBtnEl.classList.add('lrmb-playbtn-classic');
            }
            this.append(playBtnEl);
        }
        if (!playBtnEl.textContent) {
            const playBtnLabelEl = document.createElement('span');
            playBtnLabelEl.className = 'lrmb-visually-hidden';
            playBtnLabelEl.textContent = this.playLabel;
            playBtnEl.append(playBtnLabelEl);
        }

        this.addNoscriptIframe();

        playBtnEl.removeAttribute('href');

        // On hover (or tap), warm up the TCP connections we're (likely) about to use.
        this.addEventListener('pointerover', LiteRumbleEmbed.warmConnections, {once: true});

        // Once the user clicks, add the real iframe and drop our play button
        if(this.noinline) {
            this.addEventListener('click', this.openExternal);
        }
        else{
            this.addEventListener('click', this.activate);
        }
    }

    /**
     * Add a <link rel={preload | preconnect} ...> to the head
     */
    static addPrefetch(kind, url, as) {
        const linkEl = document.createElement('link');
        linkEl.rel = kind;
        linkEl.href = url;
        if (as) {
            linkEl.as = as;
        }
        document.head.append(linkEl);
    }

    /**
     * Begin pre-connecting to warm up the iframe load
     * Since the embed's network requests load within its iframe,
     *   preload/prefetch'ing them outside the iframe will only cause double-downloads.
     * So, the best we can do is warm up a few connections to origins that are in the critical path.
     *
     * Maybe `<link rel=preload as=document>` would work, but it's unsupported: http://crbug.com/593267
     * But TBH, I don't think it'll happen soon with Site Isolation and split caches adding serious complexity.
     */
    static warmConnections() {
        if (LiteRumbleEmbed.preconnected) return;

        // The iframe document and most of its subresources come right off rumble.com
        LiteRumbleEmbed.addPrefetch('preconnect', 'https://www.rumble.com');
        LiteRumbleEmbed.addPrefetch('preconnect', 'https://ak2.rmbl.ws');

        // maybe not needed?
        // LiteRumbleEmbed.addPrefetch('preconnect', 'https://rumble.cloud');
        // LiteRumbleEmbed.addPrefetch('preconnect', 'https://cdn.rumble.cloud');
        // LiteRumbleEmbed.addPrefetch('preconnect', 'https://hugh.cdn.rumble.cloud');

        LiteRumbleEmbed.preconnected = true;
    }

    // Add the iframe within <noscript> for indexability discoverability. See https://github.com/paulirish/lite-youtube-embed/issues/105
    addNoscriptIframe() {
        const iframeEl = this.createBasicIframe();
        const noscriptEl = document.createElement('noscript');
        // Appending into noscript isn't equivalant for mysterious reasons: https://html.spec.whatwg.org/multipage/scripting.html#the-noscript-element
        noscriptEl.innerHTML = iframeEl.outerHTML;
        this.append(noscriptEl);
    }

    getParams() {
        const params = new URLSearchParams(this.getAttribute('params') || []);
        params.append('autoplay', '1');
        params.append('playsinline', '1');
        return params;
    }

    async openExternal(){
        window.open(this.videoUrl, '_blank');
    }

    async activate(){
        if (this.classList.contains('lrmb-activated')) return;
        this.classList.add('lrmb-activated');

        const iframeEl = this.createBasicIframe();
        this.append(iframeEl);    
        iframeEl.focus();
    }

    createBasicIframe(){
        const iframeEl = document.createElement('iframe');
        iframeEl.width = this.width;
        iframeEl.height = this.height;
        // No encoding necessary as [title] is safe. https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html#:~:text=Safe%20HTML%20Attributes%20include
        iframeEl.title = this.title;
        iframeEl.allow = 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture';
        iframeEl.allowFullscreen = true;
        // The trailing '?' is required. Without it, the embed will fail with a CORS error.
        iframeEl.src = this.embedUrl;
        return iframeEl;
    }
}

customElements.define('lite-rumble', LiteRumbleEmbed);




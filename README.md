# Lite Rumble Embed [![NPM lite-rumble-embed package](https://img.shields.io/npm/v/lite-rumble-embed.svg)](https://npmjs.org/package/lite-rumble-embed)

A more performant and privacy-friendly way to embed Rumble videos.

Demos: https://grfr.ge/lite-rumble-embed/

## History

`lite-rumble-embed` is forked from Paul Irish's excellent [lite-youtube-embed](https://github.com/paulirish/lite-youtube-embed).

Unlike `lite-youtube-embed` which focused primarily on performance, this version for Rumble focuses more on privacy and comes with some initial ease of use trade-offs.

Things that are not implemented compared to `lite-youtube-embed`:

* thumbnail fetching. Rumble does not use predictable conventions for thumbnail URIs.
* player API. Just doesn't interest me. 

Additional implementation:

* choose classic or Rumble logo play button
* specified width and height will be honored when loading the iframe

## What you will need

The relevant Rumble video information needed for `lite-rumble-embed`:

1. videoId
2. embedId
3. thumbnail URL

While Rumble may have a philosophy more closely aligned to the ideals of a free and open internet than Youtube, it is unfortunately miles behind Youtube in terms of usability - for content consumers and producers alike.

See `./util/` for a quick and dirty tool for fetching the required information from the video ID. 

Pay attention to the difference between `videoId` and `embedId`. If you mix up the two, it's likely your video embed will report no video found.

## Basic usage

Use the [`lite-rumble-embed` npm package](https://www.npmjs.com/package/lite-rumble-embed) or download from this repo and use `src/`.

To use the custom element you will need to:

1. Include the `lite-rumble-embed` stylesheet and script within your application
1. Get the shortcodes the video you want to embed (see below)
1. Use the `lite-rumble` tag via HTML or JS
1. Be happy that you're providing a better user experience to your visitors

```html
<!-- Include the CSS & JS.. (This could be direct from the package or bundled) -->
<link rel="stylesheet" href="node_modules/lite-rumble-embed/src/lite-rumble-embed.css" />
<script src="node_modules/lite-rumble-embed/src/lite-rumble-embed.js"></script>

<!-- Use the element. You may use it before the lite-rumble-embed JS is executed. -->
<lite-rumble
  videoid="v4ndjb2"
  embedid="v4ksbkt"
  title="Play: ModelGPT - the world&apos;s first fully AI-powered mechanical keyboard"
  style="background-image: url('https://ak2.rmbl.ws/s8/1/-/N/A/W/-NAWq.qR4e-small-ModelGPT-the-worlds-first-f.jpg')"
  ></lite-rumble>
```

## Pro-usage: load w/ JS deferred (aka progressive enhancement)

Use this as your HTML, load the script asynchronously, and let the JS progressively enhance it.

Basically the same thing except an anchor and label are displayed before the script is loaded. If the video placeholder is clicked before the script is loaded, it will open the video in a new tab rather than in an iframe.

```html
    <lite-rumble 
      videoid="v4ndjb2"
      embedid="v4ksbkt"
      classicButton
      title="Play: ModelGPT - the world&apos;s first fully AI-powered mechanical keyboard"
      style="background-image: url('https://ak2.rmbl.ws/s8/1/-/N/A/W/-NAWq.qR4e-small-ModelGPT-the-worlds-first-f.jpg')">
    <a href="https://rumble.com/v4ndjb2" class="lrmb-playbtn" title="Play Video">
      <span class="lrmb-visually-hidden">Play: ModelGPT - the world&apos;s first fully AI-powered mechanical keyboard</span>
    </a>
    </lite-rumble>
```

## Mega-pro-usage: open video externally instead of in an iframe

Just add `noinline`. You can also leave out the `embedid` in this case.

```html
    <lite-rumble 
      videoid="v4ndjb2"
      noinline
      title="ModelGPT - the world&apos;s first fully AI-powered mechanical keyboard"
      style="background-image: url('https://ak2.rmbl.ws/s8/1/-/N/A/W/-NAWq.qR4e-small-ModelGPT-the-worlds-first-f.jpg')"
      ></lite-rumble>
```



## Other [third-party facades](https://web.dev/third-party-facades/)

* Lite Youtube Embed: [`paulirish/lite-youtube-embed`](https://github.com/paulirish/lite-youtube-embed)
* Lite Vimeo Embed: [`luwes/lite-vimeo-embed`](https://github.com/luwes/lite-vimeo-embed)
* &lt;lite-vimeo&gt;: [`slightlyoff/lite-vimeo`](https://github.com/slightlyoff/lite-vimeo)





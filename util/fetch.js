import rumble from "rumble-core";
const rumbleUrlPrefix = 'https://rumble.com/';

let verboseMode = process.argv.includes('-v');
function logVerbose(message) {
    if (verboseMode) { console.log(message); }
}

const basicTemplate = `<lite-rumble
  videoid="[[VIDEOID]]"
  embedid="[[EMBEDID]]"
  playlabel="Play: [[TITLE]]"
  style="background-image: url('[[THUMBNAIL]]');">
  </lite-rumble>`

  const progressiveEnhancedTemplate = `<lite-rumble 
  videoid="[[VIDEOID]]"
  embedid="[[EMBEDID]]"
  playlabel="Play: [[TITLE]]"
  style="background-image: url('[[THUMBNAIL]]');">
<a href="https://rumble.com/[[VIDEOID]]" class="lrmb-playbtn" title="Play Video">
  <span class="lrmb-visually-hidden">Play: [[TITLE]]</span>
</a>
</lite-rumble>`

function getLiteEmbedCode(description, template, videoInfo) {
  console.log(`====[ ${description} ====\n`);

  const result = template
    .replace(/\[\[EMBEDID\]\]/g, videoInfo.embedId)
    .replace(/\[\[TITLE\]\]/g, videoInfo.title)
    .replace(/\[\[THUMBNAIL\]\]/g, videoInfo.thumbnails[0].url)
    .replace(/\[\[SHORTCODE\]\]/g, videoInfo.shortcode);
  
    console.log(result + '\n');
    return result;
}

const main = async () => {
  const shortcode = process.argv[2];
  const shortUrl = rumbleUrlPrefix + shortcode;
  const info = await rumble.getInfo(shortUrl);
  const videoInfo = {
    'shortUrl': shortUrl,
    'fullUrl': rumbleUrlPrefix + info.video.pageURL,
    'videoId': shortcode,
    'embedId': info.video.privateID,
    'title': info.video.title,
    'thumbnails': info.video.thumbnails,
  }

  logVerbose(JSON.stringify(videoInfo, null, 2));

  getLiteEmbedCode("lite-rumble-embed", basicTemplate, videoInfo);
  getLiteEmbedCode("Progressive enhanced version", progressiveEnhancedTemplate, videoInfo);
}

main()
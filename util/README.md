# lite-rumble-embed helper

Just a quick and nasty helper script for getting requisite info from a Rumble video ID for using lite-rumble-embed.

## Usage

```sh
npm install
node fetch.js v4ndjb2
```

Optionally use `-v` to see more information about what is fetched from Rumble.

`v4ndjb2` provided for demo purposes only. Replace `v4ndjb2` with your own video ID accordingly.